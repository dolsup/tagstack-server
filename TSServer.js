var colors = require('colors');

var async = require('async');
var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');

var mysql = require('mysql');
var Sqlize = require('sequelize');
var sqlize = new Sqlize('TagStack', 'tagstack', 'tagstackmysql', {
  dialect: "mysql",
  port: 3306,
  define:{freezeTableName:true}
});
sqlize
  .authenticate()
  .complete(function(err) {
    if(err) {
      console.log("DB 연결 실패".red, err);
    } else {
      console.log("DB 연결 성공".red);
    }
  });



var User = sqlize.define('User', {
  username : Sqlize.STRING(50),
  email: Sqlize.STRING,
  bio: Sqlize.STRING,
  profileImage: Sqlize.BOOLEAN,
  privateView: {
    type: Sqlize.BOOLEAN,
    defaultValue: false },
  privateRecommend: {
    type: Sqlize.BOOLEAN,
    defaultValue: false },
  tagCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  viewCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  recommendCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  accessToken: Sqlize.STRING
});
var Comment = sqlize.define('Comment', {
  content: Sqlize.STRING
});
var Place = sqlize.define('Place', {
  placeName: Sqlize.STRING
});
var Theme = sqlize.define('Theme', {
  themeName: Sqlize.STRING
});
/*
var View = sqlize.define('View', {
  tagId: Sqlize.INTEGER,
  userId: Sqlize.INTEGER
});
var Recommendation = sqlize.define('Recommendation', {
  tagId: Sqlize.INTEGER,
  userId: Sqlize.INTEGER
});
*/
var Video = sqlize.define('Video', {
  address: Sqlize.STRING(15),
  tagCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  ViewCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 }
});
var Tag = sqlize.define('Tag', {
  title: Sqlize.STRING,
  description: Sqlize.STRING,
  startPoint: Sqlize.INTEGER,
  endPoint: Sqlize.INTEGER,
  repeat: Sqlize.BOOLEAN,
  private: Sqlize.BOOLEAN,
  viewCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  recommendCount: {
    type: Sqlize.INTEGER,
    defaultValue: 0 },
  profileImage: Sqlize.BLOB(15000)
});

var PlaceTag = sqlize.define('PlaceTag', {
  id: {
    type: Sqlize.INTEGER,
    primaryKey: true,
    autoINcrement: true
  }
});


Video.hasMany(Tag, {as:'tag', foreignKey:"videoId"}); //add video_id to tag
User.hasMany(Tag, {as:'tag', foreignKey:"userId"}); //add user_id to tag
User.hasMany(Comment, {as:'comment', foreignKey:"userId"});
Tag.hasMany(Comment, {as:'comment', foreignKey:"tagId"});

Tag.hasMany(Theme, {as:'theme', foreignKey:"tagId", through: 'ThemeTag'});
Theme.hasMany(Tag, {as:'tag', foreignKey:"themeId", through: 'ThemeTag'});
Tag.hasMany(Place, {as:'place', foreignKey:"tagId", through: 'PlaceTag'});
Place.hasMany(Tag, {as:'tag', foreignKey:"placeId", through: 'PlaceTag'});

User.hasMany(Tag, {as: 'view', foreignKey: 'userId', through: 'Views'});
Tag.hasMany(User, {as: 'view', foreignKey: 'tagId', through: 'Views'});

User.hasMany(Tag, {as: 'view', foreignKey: 'userId', through: 'Recommendations'});
Tag.hasMany(User, {as: 'view', foreignKey: 'tagId', through: 'Recommendations'});



Tag.sync();
User.sync();
Comment.sync();
Place.sync();
Theme.sync();
Video.sync();

/*
Tag.sync({force:true});
User.sync({force:true});
Comment.sync({force:true});
Place.sync({force:true});
//PlaceTag.sync({force:true});
//ThemeTag.sync({force:true});
Theme.sync({force:true});
Video.sync({force:true);
*/

//추천, 조회, 카운트 올리기
var app = express();

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded() );

var sty = function(json) {
  return JSON.stringify(json);
}; //stRINGIFy
var fuck = function(shit, ex) {
  console.log("FUCK YOU!!! FUCKFACE SHITHEAD? \n".rainbow + ((typeof shit === "string")?shit:shit.toString()).red + ex);
};
var mergejson = function(json1, json2) {
  return JSON.parse((JSON.stringify(json1) + JSON.stringify(json2)).replace(/}{/g,","));
};
var good = function(oh, yeah) {
  console.log(((typeof oh === "string")?oh:oh.toString()).green + "FUCKING GOOD OH YEAH!!".green + yeah.rainbow);
};

var findData = function(table, data, callback) {
  /*var condition = {};
  condition[field] = param;*/
  table
    .find({
      where: data
    })
    .complete(function(err, column) {
      if(err) {
        console.log("find 에러 :".red, err);
        return callback(err);
      } else {
        return callback(null, column);
      }
    });
};
var findAllData = function(table, field, param, callback) {
  var condition = {};
  condition[field] = param;
  table
    .findAll({
      where: condition
    })
    .complete(function(err, columns) {
      if(err) {
        console.log("find 에러 :".red, err);
        return callback(err);
      } else {
        return callback(null, columns);
      }
    });
};
var createData = function(table, data, callback) {
  table
    .create(
      data
    )
    .complete(function(err, column) {
      if(err) {
        console.log("create 에러 :".red, err);
        return callback(err);
      } else {
        console.log("create 성공", column);
        return callback(null, column);
      }
    });
};


var getSubtags = function(tag, callback) {
  var places = [];
  var themes = [];
  tag.getPlace().success(function(place) {
    for(var p in place) {
      places.push(place[p].placeName);
    }
    tag.getTheme().success(function(theme) {
      for(var t in theme) {
        themes.push(theme[t].themeName);
      }
      console.log(sty(tag)+"ASD");
      return callback(mergejson(tag, {place: places, theme: themes}));
    });
  });
};

var validateUser = function(userId, receivedToken, callback) {
  try {
    findData(User, {'id': userId}, function(err, user) {
      if(user.accessToken == receivedToken)
        return callback(null, user);
      else return callback(null, null);
    });
  } catch(ex) {
    return callback(ex);
  }
};

var imagePath = __dirname + '/public/asset/images/profiles/';
var saveProfileImage = function(user, profileImage, callback) {
  fs.writeFile(imagePath + user.id, profileImage, function(err) {
      console.log("이미지 저장 시도");
    if(err) {
      return callback(err);
    }
    user.updateAttributes({profileImage: true}, ['profileImage'])
      .success(function(user) {
        console.log(user.id," 프로필 이미지 저장 성공".green);
        return callback(null);
      })
      .error(function(err) {
        return callback(err);
      });

  });
};

/*
app.error(function(err, req, res, next) {
  if (err instanceof NotFound) {
    res.status(404).end();
  } else {
    next(err);
  }
})
*/

// 태그 뷰, 추천
// 유저.프로필사진
// 한번만 쓰는 변수 선언하지 말기
//
// 유효성 확인 메소드 분리.ok
// 컴포넌트로 활용
// 콜백 지옥 제거 (async


// C : 태그, 유저, 태그.댓글, 프로필사진
// R : 장소.태그, 주제.태그, 영상.태그, 유저.태그, 태그, 태그.댓글, 유저, *검색, 프로필사진
// U : 유저, 댓글, 태그, 프로필사진
// D : 유저, 댓글, 태그

// 태그별 품질관리용 점수

// R을 제외, CUD 명령은 모두 data.userId, data.accessToken을 포함할 것.

// user validation!!! 보거나 지울 권한 있는지 확인
// try throw catch



//Ok //유저 유효성 먼저 확인
app.post('/tag/new', function(req, res) {
  try {
    console.log("POST /tag/new".cyan);
    var data = req.body.data;
    validateUser(data.userId, data.accessToken, function(err, user) {
      if(err) throw err;
      if(user !== null) {
        createData(Tag, data, function(err, tag) {
          if(err) throw err;
          Video.findOrCreate({address: data.address})
            .success(function(video, isCreated) {
              if(isCreated) {
                console.log("새 영상 없어서 만듬 ".green+JSON.stringify(arguments).yellow);
              }
              video.addTag(tag);
              video.increment("tagCount", 1);
            }).error(function(err) {
              if(err) throw err;
            });
          user.addTag(tag);
          user.increment("tagCount", 1);
          console.log("201 새 태그 ".green+sty(arguments).yellow);
          res.status(201).end();
            // solve the callback in loop problem
          for(var p in data.place) {
            Place.findOrCreate({placeName: data.place[p]})
              .success(function(place) {
                place.addTag(tag);
              }).error(function(err) {
                throw err;
              });
          }
          for(var t in data.theme) {
            Theme.findOrCreate({themeName: data.theme[t]})
              .success(function(theme) {
                theme.addTag(tag);
              }).error(function(err) {
                throw err;
              });
          }
        });
      } else {
        fuck(404, "no such user");
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
  }

});

app.post('/user/:userId/profileImage/upload', function(req, res) {
  try {
    var userId = req.params.userId;
    var accessToken = req.query.accessToken;
    fs.readFile(req.files.image.path, function(err, imageData) {
      console.log(("POST /user/"+userId+"/profileIamge/upload").cyan);
      validateUser(userId, accessToken, function(err, user) {
        if(err) throw err;
        if(user !== null) {
          console.log("201 새 프로필 이미지 ".green);
          saveProfileImage(user, imageData, function(err) {
            if(err) throw err;
            res.status(201).end();
          });
        } else {
          fuck(404, "no such user");
          res.status(404).end();
        }
      });
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});


//Ok
app.post('/user/new', function(req, res) {
  try {
    console.log("POST /user/new".cyan, req.body.data);
    findData(User, {'accessToken': req.body.data.accessToken}, function(err, user) {
      if(err) {
        //throw err;
        console.log("q");
      }
      else if(user !== null) {
        good(200, "이미 있는 토큰");
        res.status(200).send({data: user}).end();
      } else {
        //처음 보는 액세스 토큰
        findData(User, {"email": req.body.data.email}, function(err, user) {
          if(err) {
            console.log("q");
            //throw err;
          }
          else if(user === null) {
            //유저 생성
            createData(User, req.body.data, function(err, user) {
              if(err) {
                console.log("q");
                //throw err;
              }
              else {
                console.log("201 새 유저 ".green + JSON.stringify(arguments).yellow);
                res.status(201).end();
              }
            });
          } else {
            //유저 갱신
            user.updateAttributes(req.body.data, ['accessToken', 'username'])
              .success(function(user) {
                console.log("200 이미 가입된 email, 정보 갱신 ".green);
                res.status(200).send({data: user}).end();
              });
          }
        });
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }

});

app.post('/user/:userId/refreshAccessToken', function(req, res) {
  try {
    var userId = req.params.userId;
    console.log("PUT /user/".cyan + param.cyan);
    findData(User, {'id': userId, 'accessToken': req.body.data.oldToken}, function(err, user) {
      if(err) {
        throw err;
      }
      else if (user !== null) {
        user.updateAttributes(req.body.data, ['accessToken'])
          .success(function() {
            console.log("200 user 업데이트".green + user.yellow);
            res.status(200).end();
          });
      } else {
        fuck(404);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
  }
});


app.post('/tag/:tagId/recommend', function(req, res) {
  try {
    var tagId = req.params.tagId;
    var userId = req.query.userId;
    validateUser(userId, req.body.data.accessToken, function(err, user) {
      if(user !== null) {
        console.log(("POST /tag/" + userId + "/recommend").cyan);
        findData(Tag, {'id': tagId}, function(err, tag) {
          if(err) throw err;
          else if(tag !== null) {
            good(200, "추천할 태그 찾음");
            tag.increment('recommendCount', 1);
            findData(User, {'id': userId}, function(err, user) {
              user.addRecommendations(tag);
              user.increment('recommendCount', 1);
            });
          } else {
            fuck(404, "추천할 태그 없다");
            res.status(404).end();
          }
        });
      }
    });
  } catch (ex) {
    fuck(500, ex);
  }
});

//ok
app.post('/tag/:tagId/comment/new', function(req, res) {
  try {
    console.log(("PUT /tag/" + param + "/comment/new").cyan);
    validateUser(data.userId, data.accessToken, function(err, user) {
      var tagId = req.params.tagId;
      if(err) throw err;
      else if(user !== null) {
        findData(Tag, {'id': tagId}, function(err, tag) {
          if(err) throw err;
          else if (tag !== null) {
            createData(Comment, req.body.data, function(err, comment) {
              tag.addComment(comment);
              user.addComment(comment);
              good(201,"댓글달았다");
              res.status(201).end();
            });
          } else {
            fuck("404");
          }
        });
      } else {
        fuck(404, "no such user");
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

/*

try {
  validateUser(data.userId, data.accessToken, function(err, user) {
    if(err) thorw err;
    else if(user !== null) {

    } else {
      fuck(404, "no such user");
    }
  });
} catch (ex) {
  fuck(500, ex);
  res.status(500).end();
}


try {
} catch (ex) {
  fuck(500, ex);
  res.status(500).end();
}


*/

//Ok
app.get('/place/:placeName/tags', function(req, res) {
  try {
    var placeName = req.params.placeName;
    var limit = req.query.limit;
    var offset = req.query.offset;
    console.log(("GET /place/" + placeName + "/tags").cyan);
    findData(Place, {'placeName': placeName}, function(err, place) {
      if(err) throw err;
      else if (place !== null) {
        place.getTag({where: {placeId: place.id}, limit: limit, offset: offset})
          .success(function(tags) {
            async.times(tags.length, function(n, next) {
              getSubtags(tags[n], function(newt) {
                next(err, newt);
              });
            }, function(err, tags) {
              console.log("200 장소별 태그 찾음 ".green + param.yellow);
              res.status(200).send({data: tags}).end();
            });
          });
      } else {
        console.log("404 placeName으로 tags찾다가 실패".red + err);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

//Ok
app.get('/theme/:themeName/tags', function(req, res) {
  try {
    var themeName = req.params.themeName;
    var limit = req.query.limit;
    var offset = req.query.offset;
    console.log(("GET /theme/" + themeName + "/tags").cyan);
    findData(Theme, {'themeName': themeName}, function(err, theme) {
      if(err) throw err;
      else if (theme !== null) {
        theme.getTag({where: {themeid: theme.id}, limit: limit, offset: offset})
          .success(function(tags) {
            async.times(tags.length, function(n, next) {
              getSubtags(tags[n], function(newt) {
                next(err, newt);
              });
            }, function(err, tags) {
              console.log("200 주제별 태그 찾음 ".green + param.yellow);
              res.status(200).send({data: tags}).end();
            });
          });
      } else {
        console.log("404 themeName으로 tags찾다가 실패".red + err);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});



//Ok
app.get('/video/:videoId/tags', function(req, res) {
  try {
    var videoId = req.params.videoId;
    var sort = req.query.sort;
    var limit = req.query.limit;
    var offset = req.query.offset;
    findData(Video, {'id': videoId}, function(err, video) {
      if(err) throw err;
      if(video !== null) {
        video.getTag({limit: limit, offset: offset, sort: sort})
          .success(function(tags) {
            res.status(200).send({data: tags}).end();
          })
          .error(function(err) {
            throw err;
          });
      } else {
        console.log("404 영상 없음 ".red + param.yellow);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

//OK
app.get('/user/:userId/tags', function(req, res) {
  try {
    var userId = req.params.userId;
    var limit = req.query.limit;
    var offset = req.query.offset;
    findData(User, {'id': userId}, function(err, user) {
      if(err) throw err;
      else if (user === null){
        console.log("404 유저 없음 ".red + param.yellow);
        res.status(404).end();
      } else {
        user.getTag({limit: limit, offset: offset}).success(function(usertags) {
          console.log("200 유저 태그 찾음 ".green + param.yellow);
          res.status(200).send({data: usertags}).end();
        });
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});


app.get('/user/:userId/profileImage', function(req, res) {
  try {
    var userId = req.params.userId;
    findData(User, {'id': userId}, function(err, user) {
      if(err) throw err;
      if(user !== null && user.profileImage) {
        res.sendfile(imagePath + user.id).status(200).end();
      } else {
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

//Ok
app.get('/tag/:tagId', function(req, res) {
  try {
    var tagId = req.params.tagId;
    var userId = req.query.userId;
    console.log("GET /tag/".cyan + param.yellow);
    findData(Tag, {'id': tagId}, function(err, tag) {
      if(err) throw err;
      else if (tag !== null) {
        console.log("200 태그 찾음 ".green + param.yellow);
        tag.increment('viewCount', 1);
        findData(User, {'id': userId}, function(err, user) {
          user.addView()
            .success(function(view) {
              if(view !== null) {
                user.increment('viewCount', 1);
                user.addView(tag);
              }
            });
        });
        res.status(200).send({data: getSubtags(tag)}).end();
      } else {
        console.log("404 태그 없음 ".red + param.yellow);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});


//Ok
/*
app.get('/user/:userId', function(req, res) {
  var tagId = req.params.tagId;
  console.log("GET /user/".cyan + param.yellow);
  findData(User, {'id': tagId}, function(err, user) {
    if(err) {
      console.log("500 id로 유저 찾다가 실패 ".red + param.yellow);
      res.status(500).end();
    } else if (tag === null) {
      console.log("404 유저 없음 ".red + param.yellow);
      res.status(404).end();
    } else {
      console.log("200 유저 찾음 ".green + param.yellow);
      res.status(200).send({data: user}).end();
    }
  });
});
*/

//Ok
app.get('/tag/:tagId/comments', function(req, res) {
  try {
    var tagId = req.params.tagId;
    var limit = req.query.limit;
    var offset = req.query.offset;
    console.log("PUT /tag/".cyan + param.cyan + "/comment/new");
    findData(Tag, {'id': tagId}, function(err, tag) {
      if(err) throw err;
      else if (tag !== null) {
        tag.getComment({limit: limit, offset: offset})
          .success(function(comments) {
            res.status(200).send({data: comments}).end();
          });
      } else {
        console.log("404 그 태그에 댓글 없다".yellow + err);
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

// -----Update
app.put('/user/:userId', function(req, res) {
  try {
    validateUser(data.userId, data.accessToken, function(err, user) {
      if(err) throw err;
      else if(user !== null) {
        var userId = req.params.userId;
        console.log("PUT /user/".cyan + param.cyan);
        findData(User, {'id': userId}, function(err, user) {
          if(err) throw err;
          else if(user !== null) {
            user.updateAttributes(req.body.data, ['bio', 'privateView', 'privateRecommend'])
              .success(function() {
                console.log("200 user 업데이트".green + user.yellow);
                res.status(200).end();
              });
          } else {
            console.log("404 없는 유저 접근".red + param.yellow);
            res.status(404).end();
          }
        });
      } else {
        fuck(404, "no such user");
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

app.put('/comment/:commentId', function(req, res) {
  try {
    console.log("PUT /comment/".cyan + param.cyan);
    validateUser(data.userId, data.accessToken, function(err, user) {
      if(err) throw err;
      else if(user !== null) {
        var commentId = req.params.commentId;
        findData(Comment, {'id': commentId}, function(err, comment) {
          if(err) throw err;
          else if(comment !== null) {
            comment.updateAttributes(req.body.data, ['content'])
              .success(function() {
                console.log("200 댓글 업데이트".green + comment.yellow);
                res.status(200).end();
              });
          } else {
            console.log("404 없는 댓글 접근".yellow + param.yellow);
            res.status(404).end();
          }
        });
      } else {
        fuck(404, "no such user");
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

app.put('/user/:userId/profileImage', function(req, res) {
  try {
    console.log("PUT profileImage".cyan);
    var userId = req.params.userId;
    var accessToken = req.query.accessToken;
    validateUser(userId, accessToken, function(err, user) {
      if(err) throw err;
      if(user !== null) {
        saveProfileImage(user, profileImage, function(err) {
          if(err) throw err;
          res.status(201).end();
        });
      } else {
        res.status(404).end();
      }
    });
  } catch (ex) {
    fuck(500, ex);
  }
})

//부속태그 수정기능필요
app.put('/tag/:tagId', function(req, res) {
  try {
    validateUser(data.userId, data.accessToken, function(err, user) {
      if(err) throw err;
      else if(user !== null) {
        var tagId = req.params.tagId;
        console.log("PUT /tag/".cyan + param.cyan);
        findData(Tag, {'id': tagId}, function(err, tag) {
          if(err) throw err;
          else if(tag !== null) {
            tag.updateAttributes(req.body.data, ['title', 'description', 'repeat', 'private'])
              .success(function() {
                console.log("200 태그 업데이트".green + tag.yellow);
                res.status(200).end();
              });
          } else {
            console.log("404 없는 태그 접근".yellow + param.yellow);
            res.status(404).end();
          }
        });
      } else {
        fuck(404, "no such user");
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

// -----delete
app.delete('/tag/:tagId', function(req, res) {
  try {
    validateUser(data.userId, data.accessToken, function(err, user) {
      if(err) throw err;
      else if(user !== null) {
        var tagId = req.params.tagId;
        console.log(("DELETE /tag/" + param).cyan);
        findData(Tag, {'id': tagId}, function(err, tag) {
          if(err) throw err;
          else if(tag !== null) {
            tag.destroy()
              .success(function() {
                console.log("200 태그 삭제완료".green + tag.yellow);
                res.status(200).end();
              });
          } else {
            console.log("404 지울 태그 없음".yellow + param.yellow);
            res.status(404).end();
          }
        });
      } else {
        fuck(404, "no such user");
      }
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

app.delete('/user/:userId', function(req, res) {
  try {
    var userId = req.params.userId;
    validateUser(userId, data.accessToken, function(err, user) {
      console.log(("DELETE /user/" + param).cyan);
      findData(User, {'id': userId}, function(err, user) {
        if(err) {
          console.log("500 지울 유저 찾다가 에러".red + param.yellow);
          res.status(500).end();
        } else if(user !== null) {
          user.destroy()
            .success(function() {
              console.log("200 유저 삭제완료".green + user.yellow);
              res.status(200).end();
            });
        } else {
          console.log("404 지울 유저 없음".yellow + param.yellow);
          res.status(404).end();
        }
      });
    });
  } catch (ex) {
    fuck(500, ex);
    res.status(500).end();
  }
});

app.delete('/comment/:commentId', function(req, res) {
  var commentId = req.params.commentId;
  console.log(("DELETE /comment/" + param).cyan);
  findData(Comment, {'id': commentId}, function(err, comment) {
    if(err) {
      console.log("500 지울 댓글 찾다가 에러".red + param.yellow);
      res.status(500).end();
    } else if(comment !== null) {
      comment.destroy()
        .success(function() {
          console.log("200 댓글 삭제완료".green + comment.yellow);
          res.status(200).end();
        });
    } else {
      console.log("404 지울 댓글 없음".yellow + param.yellow);
      res.status(404).end();
    }
  });
});


app.get('/form', function(req, res) {
  res.status(200).send(
      '<!DOCTYPE html>' +
      '<html>' +
        '<head>' +
          '<title> TEST UPLOAD FORM </title>' +
        '</head>' +
        '<body>' +
          '<form action="/user/:userId/profileImage", method="post", enctype="image/png">' +
            '<input type="file" name="image" />' +
            '<input type="submit" name="UPLOAD" />' +
          '</form>' +
        '</body>' +
      '</html>'
    ).end();
});




//인덱스, 아이디, 초기카운트, 종료카운트, 좋아요, 반복여부, 공개범위, 태그 내용, 댓글,
//인기태그

app.listen(3000);
console.log("Express Listening on port 3000...");
