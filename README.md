# TagStack 서버 #

STAC 2014 본선작 TagStack의 서버입니다.  
Node.js + express.js + sequelize.js + MySQL  

## **API** ##
### *Tag* ###

#### GET /tags ####
**authorization 헤더** : 로그인한 클라이언트는 보내야 한다. (선택)  
**limit, offset 쿼리** 필요  
전체 태그 리스트를 등록순으로 반환해준다.
```
{
  data: {
    [tag, tag, tag, ...
  }
}
```

#### GET /tag/:tagId ####
**authorization 헤더** : 로그인한 클라이언트는 보내야 한다. (선택)  
id가 :tagId인 태그의 정보를 반환한다.  
Views 테이블에 조회 정보가 추가된다.  
tag.viewCount, user.viewCount가 1씩 증가한다.  
place나 theme는 문자열을 : (콜론) 기준으로 끊어서 뒷부분은 앱 내 표시하는데 사용, 인덱스는 REST API에 사용.
```   
{
  data : {
    id: Sqlize.INTEGER
    title: Sqlize.STRING,
    description: Sqlize.STRING
    startPoint: Sqlize.INTEGER,
    endPoint: Sqlize.INTEGER,
    repeat: Sqlize.BOOLEAN,
    private: Sqlize.BOOLEAN,
    viewCount: Sqlize.INTEGER,
    recommendCount: Sqlize.INTEGER
    createdAt: Sqlize.DATETIME,
    updatedAt: Sqlize.DATETIME,
    videoId: Sqlize.INTEGER,
    author: Sqlize.INTEGER, //유저 id
    authorname: "string",
    place: ["string",...],
    theme: ["string",...]
  }
}
```

#### GET /place/:placeId/tags ####
**limit, offset 쿼리** 필요  
:placeId 번째 장소(장소태그)를 포함하는 태그들을 반환한다.  

```
{
  "data": [
    {
      "id": 9,
      "title": "고잔역",
      "description": "안산에 있는 고잔역.",
      "startPoint": 12,
      "endPoint": 23,
      "repeat": false,
      "private": false,
      "viewCount": 0,
      "recommendCount": 0,
      "createdAt": "2014-10-06T11:01:29.000Z",
      "updatedAt": "2014-10-06T11:01:29.000Z",
      "videoId": 2,
      "author": 1,
      "authorname": "돌숲",
      "place": [
        "1:한국",
        "2:단원구",
        "3:안산",
        "4:역"
      ],
      "theme": [
        "1:전철",
        "2:철도"
      ]
    }, { tag }, { tag }, ...
  ]
}
```

#### GET /theme/:themeId/tags ####
**limit, offset 쿼리** 필요  
:themeId 번째 주제(해시태그)를 포함하는 태그들을 반환한다.  
```
{
  "data": [
    { tag }, { tag }, ...
    ]
}
```

#### GET /video/:videoId/tags ###
**limit, offset 쿼리** 필요  
:videoId의 영상에 등록된 태그들을 반환한다.
```
{
  "data": [
    { tag }, { tag }, ...
    ]
}
```

#### GET /user/:userId/tags ###
**limit, offset 쿼리** 필요  
:userId의 유저가 등록한 태그들을 반환한다.
```
{
  "data": [
    { tag }, { tag }, ...
    ]
}
```

#### GET /tag/:tagId/comments ####
**limit, offset 쿼리** 필요  
id가 :tagId인 태그의 댓글들을 반환한다.
```
{
  "data": [
    {
      "id": 1, //댓글 ID
      "content": "댓글내용입니다.",
      "createdAt": "2014-09-18T06:56:48.000Z",
      "updatedAt": "2014-09-18T06:56:48.000Z",
      "author": 1, //작성자 ID
      "tagId": 1
    }
  ]
}
```


#### POST /tag/new ####
**authorization 헤더** 필요  
body.data의 내용대로 태그를 생성한다.  
address의 영상 정보가 DB에 없으면 생성한다.

```
{
  data : {
    title: Sqlize.STRING,
    description: Sqlize.STRING,
    address: Sqlize.STRING(20), // 유튜브 동영상 식별자
    startPoint: Sqlize.INTEGER,
    endPoint: Sqlize.INTEGER,
    repeat: Sqlize.BOOLEAN,
    private: Sqlize.BOOLEAN,
    place: ["string",...],
    theme: ["string",...]
  }
}
```


#### GET /tag/search/:word
**authorization 헤더** : 로그인한 클라이언트는 보내야 한다. (선택)  
**limit, offset 쿼리** 필요  
검색 범위 : 태그 제목, 태그 내용, 작성자 이름  
태그마다 weight 필드가 있다. 높을수록 중요한 검색결과. 
일반적으로 높은 확률로 weight가 높은 순서로 정렬되어 반환되지만, 절대적으로 순서가 보장되는 것은 아니다.
```
{
  data: {
    [tag, tag, tag, ...
  }
}
```
#### POST /videos/gettagcounts/ ####
body.data.slugs에 동영상 식별자들을 배열로 넣어 보내면 각 동영상의 DB상의 videoId와 태그 개 수를 반환한다. 아직 태그가 달린 적 없는 동영상은 null 값을 반환한다.  
```
{
  "data": 
    [
      {"slug":"qwer","id":2,"tagCount":1},
      {"slug":"asdf","id":1,"tagCount":1},
      {"slug":"ddd","id":null,"tagCount":null}
    ]
}
```
#### POST /tag/:tagId/view ####
**authorization 헤더** 필요  
해당 태그를 본다. 태그와 유저 각각 viewCount가 1씩 증가한다.  
View 테이블에 태그-유저 관계 컬럼을 생성한다.
성공 200, 이미 봄 409, 태그 없음 404, 유저 인증 실패 401, 내부 오류 500

#### POST /tag/:tagId/recommend ####
**authorization 헤더** 필요  
해당 태그를 추천한다. 태그와 유저 각각 recommendCount가 1씩 증가한다.
성공 200, 중복 409, 태그 없음 404, 유저 인증 실패 401, 내부 오류 500

#### POST /tag/:tagId/comment/new ####
**authorization 헤더** 필요  
해당 태그에 댓글을 쓴다. 등록한 댓글의 개수 속성은 따로 없다.  
성공 201, 태그 없음 404, 인증 실패 401, 내부 오류 500  
```
{
  data: {
   content:"댓글내용"
  }
}
```

#### PUT /tag/:tagId/ ####
**authorization 헤더** 필요  
body.data의 내용대로 태그를 수정한다.  
title, description, repeat, private, place, theme만 수정 가능.


#### delete /tag/:tagId/ ####
**authorization 헤더** 필요  
id가 :tagID인 태그를 삭제한다.
해당 태그에 다시 접근하면 404 에러.



### *User* ###

#### POST /user/new ####
body.data의 내용대로 유저를 생성한다.  
전달받은 액세스토큰을 가진 유저가 이미 있을 때는 해당 사용자의 정보를 반환한다.
아니라면 액세스 토큰의 유효성을 검사한 뒤 페이스북 유저 아이디를 받아온다.
그 아이디가 기존 유저와 중복되면 새로 받은 정보로 교체한 뒤 해당 사용자의 정보를 반환한다.
```
{
  data : {
    username : Sqlize.STRING(50),
    email: Sqlize.STRING,
    bio: Sqlize.STRING,
    privateView: Sqlize.BOOLEAN,
    privateRecommend: Sqlize.BOOLEAN,
    accessToken: Sqlize.STRING
  }
}
```

#### POST /user/:userId/refreshAccessToken ####
**authorization 헤더** 필요  
User 테이블에서 id가 :userId인 유저의 액세스토큰 속성을 교체한다.  
```
{
  data: {
    accessToken: "3y08usfjovy450u8weia73"
  }
}
```

#### POST /user/:userId/profileImage/upload ####
**authorization 헤더** 필요  
id가 :userId인 유저의 프로필사진(200*200 사이즈)을 업로드한다.  
User 테이블의 profileImage 속성을 true로 바꾼다.  
content-type : multipart/form-data  
이미지 Parameter Key : image  

#### GET /user/:userId/profileImage ####
id가 :userId인 유저의 프로필사진을 반환한다.

#### PUT /user/:userId ####
**authorization 헤더** 필요  
body.data의 내용대로 유저 정보를 수정한다.  
bio privateView, email, privateRecommend만 수정 가능.


### 에러 코드 ###
200 성공
201 생성 성공
404 대상이 없음
409 중복
401 인증 오류
403 권한 없음
500 서버 오류



# 지금 있는것 #

POST : 태그, 유저, 프로필사진, 댓글, 액세스토큰 교체, 추천  
GET : 장소별 태그, 주제별 태그, 동영상별 태그, 유저별 태그, 프로필사진, 태그, 태그별 댓글  
PUT : 유저, 댓글, 프로필사진, 태그  
DELETE : 태그, 유저, 댓글  
  
필요한 기능 : 검색